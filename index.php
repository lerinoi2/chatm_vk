<?php
error_reporting("E_PARSE & ~E_ERROR");

if (!isset($_REQUEST)) {
    return;
}

include_once "lib/market.php";
include_once "config.php";

$bot = new Bot();

$data = json_decode(file_get_contents('php://input'));

switch ($data->type) {
    case 'confirmation':
        echo '3c054b18';
    break;

    case "group_join":
        header("HTTP/1.1 200 OK");
        echo 'ok';
        $user_id = $data->object->user_id;
        $bot->send($user_id, "Я - чат-ассистент компании 'Chat Marketing'✋\nДавайте познакомимся поближе 🤗, а заодно я отвечу на ваши вопросы!\nНажмите 👉 Старт",'', KEYBOARD_START);
    break;

    case 'message_new':

        header("HTTP/1.1 200 OK");
        echo 'ok';

        $peer_id = $data->object->peer_id;
        $user_id = $data->object->from_id;
        $payload = json_decode($data->object->payload);
        $message = $data->object->text;
        $message = str_replace(',', '', $message);
        $message = trim(preg_replace('|\s+|', ' ', trim(preg_replace('/\[club[0-9]{1,}\|[^]]{1,}]/','',$message))));
        $message = trim(preg_replace('|\s+|', ' ', trim(preg_replace('/\[id[0-9]{1,}\|[^]]{1,}]/','',$message))));
        $message = mb_strtolower($message, "UTF-8");

        if(empty($payload)) {

            $action = json_decode($bot->getLastAction($peer_id));

            switch ($action->name){
                case "number_start":
                    $bot->setUserInfo($peer_id, "number", $message);
                    $bot->send($peer_id, "И email.", "", KEYBOARD_DEFAULT);
                    $bot->setLastAction($peer_id, json_encode(array('name' => 'email_start')));
                break;

                case "email_start":
                    $bot->setUserInfo($peer_id, 'email', $message);
                    $bot->send($peer_id, "Спасибо! Ваши контакты заботливо сохранены!", "", KEYBOARD_DEFAULT);
                    $bot->setLastAction($peer_id, ' ');
                    $bot->send($peer_id, $l['start'], "", KEYBOARD_DEFAULT);
                break;

                default:
                    if ($action->name != 'number_start' and $action->name != 'email_start'){
                        $bot->send($peer_id, "Вот эту команду я не понял, сорри!🤗 Давай попробуем еще раз? Выбери кнопку. ⬜️👈", "", KEYBOARD_DEFAULT);
                    }
                break;
            }
        } else {
            switch($payload->command) {

                case "start":
                    $bot->send($peer_id, "Пожалуйста, оставьте Ваши контакты, чтобы чат-бот мог внести Вас в список своих пользователей.", "", KEYBOARD_DEFAULT);
                    $bot->send($peer_id, "Ваш телефончик, пожалуйста!😉", "", KEYBOARD_DEFAULT);
                    $user = $bot->getUser($user_id);
                    $bot->addUser($peer_id, $user['first_name']);
                    $bot->setLastAction($peer_id, "");
                    $res = $bot->getUserInfo('number', $peer_id);
                    $bot->setLastAction($peer_id, json_encode(array('name' => 'number_start')));
                break;

                case "whatCan":
                    $bot->send($peer_id, $l['iCan'], "", KEYBOARD_DEFAULT2);
                    $bot->send($peer_id, $l['goWork'], "", KEYBOARD_DEFAULT2);
                break;

                case "goBrif":
                    $res = $bot->checkOrder($peer_id);
                    if ($res == '1'){
                        $keyboard = '{"one_time": false, "buttons": [[{"action": {"type": "text", "payload": "{\"command\": \"brifgo\"}", "label": "Да"}, "color": "positive"}, {"action": {"type": "text", "payload": "{\"command\": \"back\"}", "label": "Нет"}, "color": "negative"}]]}';
                        $bot->send($peer_id, $l['goWork'], "", KEYBOARD_DEFAULT2);
                        $bot->send($peer_id, 'Хотите изменить бриф?', "", $keyboard);
                    } else {
                        $keyboard = '{"one_time": false, "buttons": [[{"action": {"type": "text", "payload": "{\"command\": \"brif\"}", "label": "1"}, "color": "positive"}, {"action": {"type": "text", "payload": "{\"command\": \"brif\"}", "label": "2"}, "color": "positive"},{"action": {"type": "text", "payload": "{\"command\": \"brif\"}", "label": "3"}, "color": "positive"}], [{"action": {"type": "text", "payload": "{\"command\": \"brif\"}", "label": "4"}, "color": "positive"},{"action": {"type": "text", "payload": "{\"command\": \"brif\"}", "label": "5"}, "color": "positive"},{"action": {"type": "text", "payload": "{\"command\": \"back\"}", "label": "Назад"}, "color": "positive"}]]}';
                        $bot->send($peer_id, "Вид деятельности компании?", "", KEYBOARD_DEFAULT2);
                        $bot->send($peer_id, "1. ".$l['inetMagaz'], "", KEYBOARD_DEFAULT2);
                        $bot->send($peer_id, "2. ".$l['dost'], "", KEYBOARD_DEFAULT2);
                        $bot->send($peer_id, "3. ".$l['uslygi'], "", KEYBOARD_DEFAULT2);
                        $bot->send($peer_id, "4. ".$l['obraz'], "", KEYBOARD_DEFAULT2);
                        $bot->send($peer_id, "5. ".$l['restor'], "", $keyboard);
                    }
                break;

                case "brifgo":
                    $keyboard = '{"one_time": false, "buttons": [[{"action": {"type": "text", "payload": "{\"command\": \"brif\"}", "label": "1"}, "color": "positive"}, {"action": {"type": "text", "payload": "{\"command\": \"brif\"}", "label": "2"}, "color": "positive"},{"action": {"type": "text", "payload": "{\"command\": \"brif\"}", "label": "3"}, "color": "positive"}], [{"action": {"type": "text", "payload": "{\"command\": \"brif\"}", "label": "4"}, "color": "positive"},{"action": {"type": "text", "payload": "{\"command\": \"brif\"}", "label": "5"}, "color": "positive"},{"action": {"type": "text", "payload": "{\"command\": \"back\"}", "label": "Назад"}, "color": "positive"}]]}';
                    $bot->send($peer_id, "Вид деятельности компании?", "", KEYBOARD_DEFAULT2);
                    $bot->send($peer_id, "1. ".$l['inetMagaz'], "", KEYBOARD_DEFAULT2);
                    $bot->send($peer_id, "2. ".$l['dost'], "", KEYBOARD_DEFAULT2);
                    $bot->send($peer_id, "3. ".$l['uslygi'], "", KEYBOARD_DEFAULT2);
                    $bot->send($peer_id, "4. ".$l['obraz'], "", KEYBOARD_DEFAULT2);
                    $bot->send($peer_id, "5. ".$l['restor'], "", $keyboard);
                break;

                case "brif":
                    $answer = $data->object->text;
                    $bot->setBrif($peer_id, $answer, 'activity');
                    $bot->send($peer_id, "На дальнейшие вопросы отвечайте односложно - да или нет.", "", KEYBOARD_DEFAULT2);
                    $answer = $data->object->text;
                    $mes = $bot->getQuestion($answer);
                    $keyboard = '{"one_time": false, "buttons": [[{"action": {"type": "text", "payload": "{\"command\": \"answer\"}", "label": "Да"}, "color": "positive"}, {"action": {"type": "text", "payload": "{\"command\": \"answer\"}", "label": "Нет"}, "color": "negative"}]]}';
                    $bot->send($peer_id, $mes, "", $keyboard);

                    $bot->setLastAction($peer_id, json_encode(array('question' => '1', 'type' => $answer)));
                break;

                case "answer":
                    $answer = $data->object->text;
                    $anw = $answer == 'Да' ? '1' : '0';
                    $question = json_decode($bot->getLastAction($peer_id));
                    $newQuesrtion = (int)$question->question + 1;
                    $bot->setBrif($peer_id, $anw,'q'.$question->question);
                    $mes = $bot->getQuestion($question->type, 'q'.$newQuesrtion);

                    if ($newQuesrtion == 3 and $question->type == 1){
                        $keyboard = '{"one_time": false, "buttons": [[{"action": {"type": "text", "payload": "{\"command\": \"answer5\"}", "label": "Да"}, "color": "positive"}, {"action": {"type": "text", "payload": "{\"command\": \"answer5\"}", "label": "Нет"}, "color": "negative"}]]}';
                    } elseif ($newQuesrtion == 4 and $question->type != 1) {
                        $keyboard = '{"one_time": false, "buttons": [[{"action": {"type": "text", "payload": "{\"command\": \"answer5\"}", "label": "Да"}, "color": "positive"}, {"action": {"type": "text", "payload": "{\"command\": \"answer5\"}", "label": "Нет"}, "color": "negative"}]]}';
                    } else {
                        $keyboard = '{"one_time": false, "buttons": [[{"action": {"type": "text", "payload": "{\"command\": \"answer\"}", "label": "Да"}, "color": "positive"}, {"action": {"type": "text", "payload": "{\"command\": \"answer\"}", "label": "Нет"}, "color": "negative"}]]}';
                    }

                    $bot->send($peer_id, $mes, "", $keyboard);
                    $bot->setLastAction($peer_id, json_encode(array('question' => $newQuesrtion, 'type' => $question->type)));
                break;

                case "moreQuestions":
                    $keyboard = '{"one_time": false, "buttons": [[{"action": {"type": "text", "payload": "{\"command\": \"where\"}", "label": "Где применить чат-бот?"}, "color": "positive"}, {"action": {"type": "text", "payload": "{\"command\": \"etap\"}", "label": "Этапы создания чат-бота"}, "color": "positive"}],[{"action": {"type": "text", "payload": "{\"command\": \"howMatch\"}", "label": "Сколько стоит?"}, "color": "positive"},{"action": {"type": "text", "payload": "{\"command\": \"dov\"}", "label": "Что доверить чат-боту?"}, "color": "positive"}, {"action": {"type": "text", "payload": "{\"command\": \"back\"}", "label": "Назад"}, "color": "positive"}]]}';
                    $bot->send($peer_id, $l['chtoras'], "", $keyboard);
                break;

                case "answer5":
                    $question = json_decode($bot->getLastAction($peer_id));
                    $answer = $data->object->text;
                    $anw = $answer == 'Да' ? '1' : '0';
                    $bot->setBrif($peer_id, $anw,'q'.$question->question);
                    $res = $bot->getUserInfo('number', $peer_id);
                    if (is_null($res)){
                        $bot->send($peer_id, "Нам очень важны Ваши ответы. Пожалуйста, оставьте Ваши контакты, а мы заботливо сохраним их, чтобы уточнить информацию.", "", KEYBOARD_DEFAULT2);
                        $bot->send($peer_id, $l['gophone'], "", KEYBOARD_DEFAULT2);
                        $bot->setLastAction($peer_id,  json_encode(array('name' => 'number')));
                    } else {
                        $bot->send($peer_id, $l['aleksandr'], "", KEYBOARD_DEFAULT);
                        $bot->setLastAction($peer_id, ' ');
                        $bot->sendMail($peer_id);
                    }
                break;

                case 'where':
                    $keyboard = '{"one_time": false, "buttons": [[{"action": {"type": "text", "payload": "{\"command\": \"where\"}", "label": "Где применить чат-бот?"}, "color": "positive"}, {"action": {"type": "text", "payload": "{\"command\": \"etap\"}", "label": "Этапы создания чат-бота"}, "color": "positive"}],[{"action": {"type": "text", "payload": "{\"command\": \"howMatch\"}", "label": "Сколько стоит?"}, "color": "positive"},{"action": {"type": "text", "payload": "{\"command\": \"dov\"}", "label": "Что доверить чат-боту?"}, "color": "positive"}, {"action": {"type": "text", "payload": "{\"command\": \"back\"}", "label": "Назад"}, "color": "positive"}]]}';
                    $bot->send($peer_id, $l['whereText'], "", $keyboard);
                    $bot->send($peer_id, $l['inetMagaz']."\n\n".$l['dost']."\n\n".$l['uslygi']."\n\n".$l['restor']."\n\n".$l['obraz'], "", $keyboard);
                break;

                case 'dov':
                    $keyboard = '{"one_time": false, "buttons": [[{"action": {"type": "text", "payload": "{\"command\": \"where\"}", "label": "Где применить чат-бот?"}, "color": "positive"}, {"action": {"type": "text", "payload": "{\"command\": \"etap\"}", "label": "Этапы создания чат-бота"}, "color": "positive"}],[{"action": {"type": "text", "payload": "{\"command\": \"howMatch\"}", "label": "Сколько стоит?"}, "color": "positive"},{"action": {"type": "text", "payload": "{\"command\": \"dov\"}", "label": "Что доверить чат-боту?"}, "color": "positive"}, {"action": {"type": "text", "payload": "{\"command\": \"back\"}", "label": "Назад"}, "color": "positive"}]]}';
                    $bot->send($peer_id, $l['dovText'], "", $keyboard);
                    $bot->send($peer_id, $l['dovText2'], "", $keyboard);
                break;

                case 'etap':
                    $keyboard = '{"one_time": false, "buttons": [[{"action": {"type": "text", "payload": "{\"command\": \"where\"}", "label": "Где применить чат-бот?"}, "color": "positive"}, {"action": {"type": "text", "payload": "{\"command\": \"etap\"}", "label": "Этапы создания чат-бота"}, "color": "positive"}],[{"action": {"type": "text", "payload": "{\"command\": \"howMatch\"}", "label": "Сколько стоит?"}, "color": "positive"},{"action": {"type": "text", "payload": "{\"command\": \"dov\"}", "label": "Что доверить чат-боту?"}, "color": "positive"}, {"action": {"type": "text", "payload": "{\"command\": \"back\"}", "label": "Назад"}, "color": "positive"}]]}';
                    $bot->send($peer_id, $l['etapText'], "", $keyboard);
                break;

                case 'howMatch':
                    $keyboard = '{"one_time": false, "buttons": [[{"action": {"type": "text", "payload": "{\"command\": \"where\"}", "label": "Где применить чат-бот?"}, "color": "positive"}, {"action": {"type": "text", "payload": "{\"command\": \"etap\"}", "label": "Этапы создания чат-бота"}, "color": "positive"}],[{"action": {"type": "text", "payload": "{\"command\": \"howMatch\"}", "label": "Сколько стоит?"}, "color": "positive"},{"action": {"type": "text", "payload": "{\"command\": \"dov\"}", "label": "Что доверить чат-боту?"}, "color": "positive"}, {"action": {"type": "text", "payload": "{\"command\": \"back\"}", "label": "Назад"}, "color": "positive"}]]}';
                    $bot->send($peer_id, 'Базовый чат-бот стоит от 15 000 р.', "", $keyboard);
                    $bot->send($peer_id, $l['howMatchText'], "", $keyboard);
                    $bot->send($peer_id,$l['goBrifi'], "", KEYBOARD_DEFAULT2);
                break;

                case 'home':
                case 'back':
                    $bot->send($peer_id, 'Назад', "", KEYBOARD_DEFAULT);
                    $bot->setLastAction($peer_id, "");
                break;

                case "buy":
                    $bot->send($peer_id, $l['goBrifi'], "", KEYBOARD_DEFAULT);
                    $keyboard = '{"one_time": false, "buttons": [[{"action": {"type": "text", "payload": "{\"command\": \"call\"}", "label": "Позвонить"}, "color": "positive"}, {"action": {"type": "text", "payload": "{\"command\": \"text\"}", "label": "Написать"}, "color": "positive"}],[{"action": {"type": "text", "payload": "{\"command\": \"home\"}", "label": "На главную"}, "color": "positive"}]]}';
                    $bot->send($peer_id, $l['callMeText'], "", $keyboard);
                    $bot->setLastAction($peer_id, ' ');
                break;

                case "call":
                    $res = $bot->getUserInfo('number', $peer_id);
                    if (is_null($res)) {
                        $bot->send($peer_id, "Уважаю классику!", "", KEYBOARD_DEFAULT);
                        $bot->send($peer_id, $l['gophone'], "", KEYBOARD_DEFAULT);
                        $bot->setLastAction($peer_id, 'number');
                    } else {
                        $bot->sendMail($peer_id, "call");
                        $bot->send($peer_id, $l['aleksandr2'], "", KEYBOARD_DEFAULT);
                    }
                break;

                case "text":
                    $res = $bot->getUserInfo('number', $peer_id);
                    if (is_null($res)) {
                        $bot->send($peer_id, $l['gophone'], "", KEYBOARD_DEFAULT);
                        $bot->setLastAction($peer_id, 'number');
                    } else {
                        $bot->sendMail($peer_id, "text");
                        $bot->send($peer_id, $l['aleksandr2'], "", KEYBOARD_DEFAULT);
                    }
                break;

                case 'price':
                    $bot->send($peer_id, 'Минимальная стоимость чат-бота - 15 000 р.', "", KEYBOARD_DEFAULT);
                    $keyboard = '{"one_time": false, "buttons": [[{"action": {"type": "text", "payload": "{\"command\": \"brif\"}", "label": "Продолжить"}, "color": "positive"},{"action": {"type": "text", "payload": "{\"command\": \"callMe\"}", "label": "Свяжитесь со мной"}, "color": "positive"}],[{"action": {"type": "text", "payload": "{\"command\": \"whatCan\"}", "label": "Что умеет бот"}, "color": "positive"},{"action": {"type": "text", "payload": "{\"command\": \"buy\"}", "label": "Заказать бота"}, "color": "positive"}],[{"action": {"type": "text", "payload": "{\"command\": \"price\"}", "label": "Стоимость"}, "color": "positive"}, {"action": {"type": "text", "payload": "{\"command\": \"call\"}", "label": "Свяжитесь со мной"}, "color": "positive"}]]}';
                    $bot->send($peer_id, $l['priceText'], "", $keyboard);
                break;

                case 'callMe':
                    $keyboard = '{"one_time": false, "buttons": [[{"action": {"type": "text", "payload": "{\"command\": \"call\"}", "label": "Позвонить"}, "color": "positive"}, {"action": {"type": "text", "payload": "{\"command\": \"text\"}", "label": "Написать"}, "color": "positive"}],[{"action": {"type": "text", "payload": "{\"command\": \"home\"}", "label": "На главную"}, "color": "positive"}]]}';
                    $bot->send($peer_id, $l['callMeText'], "", $keyboard);
                break;
            }
        }
    break;

break;
}
