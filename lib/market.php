<?php
include_once "locale.php";
include_once "vk-api.php";

class Bot extends Api
{
    private  $host = 'localhost:3306';
    private  $dbname = 'chatm';
    private  $login = 'phpmyadmin';
    private  $password = 'some_pass';
    private  $charset = 'utf8';
    private  $connection;

    function getDB ($query)
    {
        $out = "";
        $this->connection = "mysql:host=$this->host;dbname=$this->dbname;charset=$this->charset";
        try
        {
            $db = new PDO($this->connection, $this->login, $this->password);
        }
        catch (PDOException $e)
        {
            echo $e->getMessage();
        }
        $stmt = $db->prepare($query);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
        {
            $out[] = $row;
        }
        return $out ? $out : false;
    }

    public function addUser($chatId, $name)
    {
        $res = $this->getDB("SELECT `chat_id` FROM chatm.users WHERE `chat_id` = '".$chatId."'");
        if (is_null($res[0]['chat_id'])){
            $this->getDB("INSERT INTO `chatm`.`users` (`chat_id`, `name`, `date_add`) VALUES ('".$chatId."', '".$name."', '".time()."')");
        }
    }

    public function setLastAction($chatId, $lastAction){
        $res = $this->getDB("UPDATE `chatm`.`users` SET last_action = '".$lastAction."' WHERE `chat_id` = '".$chatId."'");
        return $res;
    }

    public function getLastAction($chatId){
        $res = $this->getDB("SELECT `last_action` FROM chatm.users WHERE `chat_id` = '".$chatId."'");
        return $res[0]['last_action'];
    }

    public function getQuestion($id, $number = 'q1'){
        $res = $this->getDB("SELECT `".$number."` FROM chatm.active WHERE `id` = '".$id."'");
        return $res[0][$number];
    }

    public function setBrif($chatId, $data, $type){
        $res = $this->getDB("SELECT `activity` FROM chatm.brif WHERE `chat_id` = '".$chatId."'");
        if (is_null($res) || !$res){
            $this->getDB("INSERT INTO `chatm`.`brif` (`".$type."`, `chat_id`) VALUES ('".$data."', '".$chatId."')");
        } else {
            $this->getDB("UPDATE `chatm`.`brif` SET ".$type." = '".$data."' WHERE `chat_id` = '".$chatId."'");
        }
    }

    public function setUserInfo($chatId, $field, $data){
        $this->getDB("UPDATE `chatm`.`users` SET ".$field." = '".$data."' WHERE `chat_id` = '".$chatId."'");
    }

    public function getUserInfo($field, $chatId){
        $res = $this->getDB("SELECT `".$field."` FROM chatm.users WHERE `chat_id` = '".$chatId."'")[0][$field];
        return $res;
    }

    public function getAllUserInfo($chatId){
        $res = $this->getDB("SELECT `name`, `number`, `email` FROM chatm.users WHERE `chat_id` = '".$chatId."'")[0];
        return $res;
    }

    public function getAction(){
        $res = $this->getDB("SELECT `type` FROM chatm.active");
        return $res;
    }

    public function sendMail($chatId, $type = "vk test")
    {
        $this->getDB("UPDATE `chatm`.`users` SET orders = '1' WHERE `chat_id` = '" . $chatId . "'");
        $res = $this->getDB("SELECT `activity` FROM chatm.brif WHERE `chat_id` = '" . $chatId . "'")[0]['activity'];
        $user = $this->getDB("SELECT `name`, `number`, `email` FROM chatm.users WHERE `chat_id` = '" . $chatId . "'")[0];
        $activity = $this->getDB("SELECT `type` FROM chatm.active WHERE `id` = '" . $res . "'")[0];
        $qwe = $this->getDB("SELECT `q1`, `q2`, `q3`, `q4`, `q5` FROM chatm.brif WHERE `chat_id` = '" . $chatId . "'")[0];
        $i = 1;
        $mes = "";
        foreach ($qwe as $value) {
            $text = $this->getDB("SELECT `q" . $i . "` FROM chatm.active WHERE `id` = '" . $res . "'")[0];
            $l = "q" . $i;
            $q = $value[$l] == "1" ? "Да" : "Нет";
            $text = $text[$l] ? $text[$l] : 'nope';
            if ($text == 'nope'){
                $i++;
            } else {
                $mes .= $text . " : " . $q . "\n";
                $i++;
            }
        }
        $type = $type == 'call' ? "Позвонить vk test" : "Написать vk test";
//        $data = [
//            'email' => $user['email'],
//            'phone' => $user['number'],
//        ];

//        $options = [
//            CURLOPT_URL => 'http://bot.delive.me/chatM_bot/telegram/lib/test.php',
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_POST => true,
//            CURLOPT_POSTFIELDS => $data,
//        ];

        $curl = curl_init();
//        curl_setopt_array($curl, $options);
//        $res = curl_exec($curl);

        $chatId = $this->getDB("SELECT `chat_id` FROM `chatm`.`users` WHERE `flow`='1'");

        foreach ($chatId as $value){
            foreach ($value as $item) {
                $chats[]= $item;
            }
        }

        $bron = [
            'Тип ' => $type ? $type : 'nope',
            'Клиент ' => $user['name'] ? $user['name'] : 'nope',
            'Выбранная сфера ' => $activity['type'] ? $activity['type'] : 'nope',
            'Бриф ' => $mes ? $mes : 'nope',
            'Контактный телефон ' => $user['number'] ? $user['number'] : 'nope',
            'Контактный email ' => $user['email'] ? $user['email'] : 'nope',
        ];

        $data = [
            'send' => '1',
            'chatid' => json_encode($chats),
            'data' => json_encode($bron),
        ];

        $options = [
            CURLOPT_URL => 'https://bot.delive.me/chatM_bot/telegram/admin/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data,
        ];

        curl_setopt_array($curl, $options);
        curl_exec($curl);
        curl_close($curl);

    }

    public function checkOrder($chatId){
        $res = $this->getDB("SELECT `orders` FROM chatm.users WHERE `chat_id` = '".$chatId."'")[0]['orders'];
        return $res;
    }

    public function setAnswer($id, $text, $chatId){
        $this->getDB("INSERT INTO `chatm`.`answers` (`id_int`, `text`, `chat_id`) VALUES ('".$id."', '".$text."', '".$chatId."')");
    }

    public function getAllUsers(){
        $res = $this->getDB("SELECT `chat_id` FROM chatm.users WHERE `platform` = '1'");
        return $res;
    }
}
?>