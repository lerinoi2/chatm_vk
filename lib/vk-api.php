<?php
class Api
{

    function vkapi($method, $params = array())
    {
        if (!isset($params['access_token'])) $params['access_token'] = 'ae6d92821fabba52e6861a7ee4a2559dd6489a4dbecda4b30b69558382ba2df82a53227416e2b316ab1ad';
        $params['v'] = VKAPI_VERSION;
        $query = http_build_query($params);
        $url = 'https://api.vk.com/method/' . $method . '?' . $query;
        $response = json_decode(file_get_contents($url), true);
        return $response;
    }

    function send($peer_id, $message, $attachment = '', $keyboard = '{"one_time": false, "buttons": []}')
    {
        $params['peer_id'] = $peer_id;
        $params['message'] = $message;
        if ($attachment != ''){
            $params['attachment'] = implode(',', $attachment);
        } else {
            $params['attachment'] = $attachment;
        }
        if ($peer_id < 2000000000) $params['keyboard'] = $keyboard;
        $params['access_token'] = 'ae6d92821fabba52e6861a7ee4a2559dd6489a4dbecda4b30b69558382ba2df82a53227416e2b316ab1ad';
        $params['v'] = "5.80";
        $get_params = http_build_query($params);

        $response = json_decode(file_get_contents('https://api.vk.com/method/messages.send?' . $get_params));
        if (isset($response->error)) return false;

        return true;
    }

    function sendLoc($peer_id, $message, $lat, $lon, $user_id, $keyboard = '{"one_time": false, "buttons": []}')
    {

        $params['peer_id'] = $user_id;
        $params['user_id'] = $peer_id;
        $params['message'] = $message;

        $params['lat'] = $lat;
        $params['long'] = $lon;

        if ($peer_id < 2000000000) $params['keyboard'] = $keyboard;
        $params['access_token'] = 'ae6d92821fabba52e6861a7ee4a2559dd6489a4dbecda4b30b69558382ba2df82a53227416e2b316ab1ad';
        $params['v'] = "5.80";
        $get_params = http_build_query($params);

        $response = json_decode(file_get_contents('https://api.vk.com/method/messages.send?' . $get_params));
        if (isset($response->error)) return false;

        return true;
    }


    function getUser($user_id, $fields = 'sex', $name_case = 'nom')
    {
        $response = $this->vkapi('users.get', ['user_ids' => $user_id, 'fields' => $fields, 'name_case' => $name_case]);

        return $response['response'][0];
    }

    function vkApi_upload($url, $file_name) {
        if (!file_exists($file_name)) {
            throw new Exception('File not found: '.$file_name);
        }
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, array('file' => new CURLfile($file_name)));
        $json = curl_exec($curl);
        $error = curl_error($curl);
        if ($error) {
            throw new Exception("Failed {$url} request");
        }
        curl_close($curl);
        $response = json_decode($json, true);
        if (!$response) {
            throw new Exception("Invalid response for {$url} request");
        }
        return $response;
    }

    function vkApi_photosGetMessagesUploadServer($peer_id) {
        return $this->_vkApi_call('photos.getMessagesUploadServer', ['peer_id' => $peer_id]);
    }

    function vkApi_photosSaveMessagesPhoto($photo, $server, $hash) {
        return $this->_vkApi_call('photos.saveMessagesPhoto',['photo'  => $photo,'server' => $server,'hash' => $hash]);
    }

    function _bot_uploadPhoto($user_id, $file_name) {
        $upload_server_response = $this->vkApi_photosGetMessagesUploadServer($user_id);
        $upload_response = $this->vkApi_upload($upload_server_response['upload_url'], $file_name);
        $photo = $upload_response['photo'];
        $server = $upload_response['server'];
        $hash = $upload_response['hash'];
        $save_response = $this->vkApi_photosSaveMessagesPhoto($photo, $server, $hash);
        $photo = array_pop($save_response);
        return $photo;
    }

    function _vkApi_call($method, $params = array()) {
        $params['access_token'] = 'ae6d92821fabba52e6861a7ee4a2559dd6489a4dbecda4b30b69558382ba2df82a53227416e2b316ab1ad';
        $params['v'] = "5.80";
        $query = http_build_query($params);
        $url = 'https://api.vk.com/method/'.$method.'?'.$query;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($curl);
        $error = curl_error($curl);
        if ($error) {
            throw new Exception("Failed {$method} request");
        }
        curl_close($curl);
        $response = json_decode($json, true);
        if (!$response || !isset($response['response'])) {
            throw new Exception("Invalid response for {$method} request");
        }
        return $response['response'];
    }

}

?>